function onlyNumbers(evt) {
  let e = event || evt; // For trans-browser compatibility
  let charCode = e.which || e.keyCode;

  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}

function NumToWord(inputNumber, outputControl) {
  let str = new String(inputNumber);

  let numSplit = str.split("");
  let numRev = numSplit.reverse();
  let once = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
  let twos = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
  let tens = ['', 'Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];

  numLength = numRev.length;
  let word = new Array();
  let j = 0;

  for (i = 0; i < numLength; i++) {
      switch (i) {

          case 0:
              if ((numRev[i] == 0) || (numRev[i + 1] == 1)) {
                  word[j] = '';
              }
              else {
                  word[j] = '' + once[numRev[i]];
                  console.log(word[j]);
                  
              }
              word[j] = word[j];
              break;

          case 1:
              tenplus();
              break;

          case 2:
              if (numRev[i] == 0) {
                  word[j] = '';
              }
              else if ((numRev[i - 1] == 0) || (numRev[i - 2] == 0)) {
                  word[j] = once[numRev[i]] + " Hundred ";
                  console.log(word[j]);

              }
              else {
                  word[j] = once[numRev[i]] + " Hundred and";
                  console.log(word[j]);

              }
              break;

          case 3:
              if (numRev[i] == 0 || numRev[i + 1] == 1) {
                  word[j] = '';
              }
              else {
                  word[j] = once[numRev[i]];
                  console.log(word[j]);

              }
              if ((numRev[i + 1] != 0) || (numRev[i] > 0)) {
                  word[j] = word[j] + " Thousand";
                  console.log(word[j]);

              }
              break;

          case 8:
              tenplus();
              break;

      }
      j++;
  }

  function tenplus() {
      if (numRev[i] == 0) { word[j] = ''; }
      else if (numRev[i] == 1) { word[j] = twos[numRev[i - 1]]; }
      else { word[j] = tens[numRev[i]]; }
  }
  word.reverse();
  let outputWord = '';
  for (i = 0; i < numLength; i++) {
      outputWord = outputWord + word[i];
  }
  document.getElementById(outputControl).innerHTML = outputWord;
}