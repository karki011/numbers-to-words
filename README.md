# Instructions
### Write a function named numbersToWords that returns an array with all the numbers from 1 through 1000 in words, e.g. "one hundred seventeen" or "three hundred forty two".

### Start off small by only handling the numbers from 1 to 20 at first. (1 points)

### Once you've figured that out, extend your solution to work for the numbers up to 100. (3 points)

### If you've solved that, continue on to the full solution handling all the numbers up to 1000. (6 points)

### Once you are successfully returning all the numbers, use JavaScript to display them on a page. Write a seperate helper function to display the results of calling numbersToWords on the page.